package main

import (
	"errors"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/cloudflare/cloudflare-go"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/version"
	"github.com/urfave/cli"
)

func recordMetrics(conf *config) func(c *cli.Context) error {
	return func(c *cli.Context) error {
		if err := parseFlags(conf, c); err != nil {
			cli.ShowAppHelp(c)
			log.Fatal(err)
			return err

		}
		go func() {
			for {

				var startDate = time.Now().Add(time.Duration(-4) * time.Minute).Format(time.RFC3339)
				var endDate = time.Now().Add(time.Duration(-2) * time.Minute).Format(time.RFC3339)
				log.Println(startDate)
				log.Println(endDate)
				if conf.dataset == "http" {
					// Construct a new API object
					api, err := cloudflare.New(conf.apiKey, conf.apiEmail)
					if err != nil {
						log.Println(err)
					}
					zones, err := api.ListZones()
					if err != nil {
						log.Println("Listing zone errored: ", err)
					}
					for _, zone := range zones {
						if zone.Plan.ZonePlanCommon.Name == "Enterprise Website" {
							log.Println(zone.Name)
							resp, err := getCloudflareHTTPMetrics(buildHttpGraphQLQuery(startDate, endDate, zone.ID), conf.apiEmail, conf.apiKey)
							if err == nil {
								for _, node := range resp.Viewer.Zones[0].Caching {
									requestBytes.WithLabelValues(node.Dimensions.CacheStatus, zone.Name).Add(float64(node.SumEdgeResponseBytes.EdgeResponseBytes))
								}
								for _, node := range resp.Viewer.Zones[0].ResponseCodes {
									requestResponseCodes.WithLabelValues(strconv.Itoa(node.SumResponseStatus.ResponseStatusMap[0].EdgeResponseStatus), zone.Name).Add(float64(node.SumResponseStatus.ResponseStatusMap[0].Requests))
								}
								log.Println(resp)
								log.Println("Fetch done at:", startDate)
								fetchDone.Inc()
							} else {
								log.Println("Fetch failed :", err)
								fetchFailed.Inc()
							}
						}
					}
				}
				if conf.dataset == "network" {
					// Construct a new API object
					api, err := cloudflare.New(conf.apiKey, conf.apiEmail)
					if err != nil {
						log.Println(err)
					}
					account, _, err := api.Account(conf.accountID)
					if err != nil {
						log.Println("Listing account errored: ", err)
					}
					resp, err := getCloudflareNetworkMetrics(buildNetworkGraphQLQuery(startDate, endDate, conf.accountID), conf.apiEmail, conf.apiKey)
					if err == nil {
						for _, node := range resp.NetworkViewer.Accounts[0].AttackHistory {
							networkBits.WithLabelValues(node.NetworkDimensions.AttackID, account.Name, node.NetworkDimensions.AttackProtocol, node.NetworkDimensions.AttackMitigationType, node.NetworkDimensions.ColoCountry, strconv.Itoa(node.NetworkDimensions.DestinationPort), node.NetworkDimensions.AttackType).Add(float64(node.Sum.Bits))
							networkPackets.WithLabelValues(node.NetworkDimensions.AttackID, account.Name, node.NetworkDimensions.AttackProtocol, node.NetworkDimensions.AttackMitigationType, node.NetworkDimensions.ColoCountry, strconv.Itoa(node.NetworkDimensions.DestinationPort), node.NetworkDimensions.AttackType).Add(float64(node.Sum.Packets))
						}
						log.Println(resp)
						log.Println("Fetch done at: ", startDate, " for:", account.Name)
						fetchDone.Inc()
					} else {
						log.Println("Fetch failed :", err)
						fetchFailed.Inc()
					}
				}
				time.Sleep(60 * time.Second)
			}
		}()
		return nil
	}
}

var (
	fetchFailed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "cloudflare_failed_fetches",
		Help: "The total number of failed fetches",
	})
	fetchDone = promauto.NewCounter(prometheus.CounterOpts{
		Name: "cloudflare_done_fetches",
		Help: "The total number of done fetches",
	})
	requestBytes = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "cloudflare_processed_bytes",
		Help: "The total number of processed bytes, labelled per cache status",
	},
		[]string{"cacheStatus", "zoneName"},
	)
	requestResponseCodes = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "cloudflare_requests_per_response_code",
		Help: "The total number of request, labelled per HTTP response codes",
	},
		[]string{"responseCode", "zoneName"},
	)
	networkBits = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "cloudflare_network_bits",
		Help: "Number of bits, labelled per AttackID",
	},
		[]string{"attackID", "accountName", "attackProtocol", "mitigationType", "country", "destinationPort", "attackType"},
	)
	networkPackets = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "cloudflare_network_packets",
		Help: "Number of packets, labelled per AttackID",
	},
		[]string{"attackID", "accountName", "attackProtocol", "mitigationType", "country", "destinationPort", "attackType"},
	)
)

func main() {
	log.SetPrefix("[cloudflare-exporter] ")
	log.SetFlags(log.Ltime)
	log.SetOutput(os.Stderr)

	app := cli.NewApp()
	app.Name = "cloudflare-exporter"
	app.Usage = "export Cloudflare metrics to prometheus"
	app.Flags = flags

	conf := &config{}
	prometheus.MustRegister(version.NewCollector("cloudflare_exporter"))

	app.Action = recordMetrics(conf)

	if err := app.Run(os.Args); err != nil {
		log.Println(err)
	}
	http.Handle("/metrics", promhttp.Handler())
	err := http.ListenAndServe(":2112", nil)
	if err != nil {
		log.Fatal(err)
	}
}

var flags = []cli.Flag{
	cli.StringFlag{
		Name:  "api-key",
		Usage: "Your Cloudflare API token",
	},
	cli.StringFlag{
		Name:  "api-email",
		Usage: "The email address associated with your Cloudflare API token and account",
	},
	cli.StringFlag{
		Name:  "dataset",
		Usage: "The data source you want to export, valid values are: http, network",
	},
	cli.StringFlag{
		Name:  "accountID",
		Usage: "Account tag to be fetched",
	},
}

type config struct {
	apiKey    string
	apiEmail  string
	dataset   string
	accountID string
}

func parseFlags(conf *config, c *cli.Context) error {
	conf.apiKey = c.String("api-key")
	conf.apiEmail = c.String("api-email")
	conf.dataset = c.String("dataset")
	conf.accountID = c.String("accountID")

	return conf.Validate()
}

func (conf *config) Validate() error {

	if conf.apiKey == "" || conf.apiEmail == "" {
		return errors.New("Must provide both api-key and api-email")
	}
	if conf.dataset == "" {
		conf.dataset = "http"
	}
	if conf.dataset != "http" && conf.dataset != "network" {
		return errors.New("Invalid dataset (valid are: http, network)")
	}
	if conf.dataset == "network" && conf.accountID == "" {
		return errors.New("You must provide an accountID when exporting network analytics")
	}

	return nil
}
